float input[4];
float output[4] = {0, 0, 0, 0};
float flow[4];
float voltage[4];
float offset[4] = {1.11, 1.07, 1.11, 1.11};
int PIN[4] = {A0, A1, A2, A3};
int cycle = 0;
//unsigned long time;
float moyenne[4] = {0, 0, 0, 0};
int counter = 0;

void setup() {
  // put your setup code here, to run once:

  Serial.begin(115200) ;

  pinMode(13, OUTPUT);

  pinMode(A0, INPUT);
  pinMode(A1, INPUT);
  pinMode(A2, INPUT);
  pinMode(A3, INPUT);
}


void loop() {
  // put your main code here, to run repeatedly:
  while (cycle < 1000){
    cycle++;

    for (int i=0; i<4; i++) {
      input[i] = (float) analogRead(PIN[i]);
      output[i] = 0.99f * output[i] + 0.01f * input[i];
    }
  }

  cycle=0;

  for (int j=0; j<4; j++) {
    voltage[j] = (output[j] / 1023) * 5;
    flow[j] = (voltage[j] - offset[j]) / 0.186;
    Serial.println(String("Voltage ") + (j+1) + String(" : ") + voltage[j]);
    //Serial.println(String("Flow ") + (j+1) + String(" (SLPM) : "  ) + flow[j]);
    //time = millis()/1000;
    //Serial.println("Time (s) ");
    //Serial.println(time );
  }
  Serial.println();
}
