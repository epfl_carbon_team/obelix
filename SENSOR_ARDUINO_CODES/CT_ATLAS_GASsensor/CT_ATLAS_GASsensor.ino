
//This code will work on an Arduino Uno and Mega
//This code was written to be easy to understand.
//Modify this code as you see fit.
//This code will output data to the Arduino serial monitor.
//Type commands into the Arduino serial monitor to control the EZO Co2 sensor.
//This code was written in the Arduino 1.8.9 IDE
//This code was last tested 7/2019


#include <Wire.h> //enable I2C.

//colonne1
#define address 105 // co2 - 1
#define address2 46 // o2 - 1 
#define address3 22 // hum - 1

//colonne2
#define address4 28 // co2 - 2
#define address5 45 // o2 - 2 
#define address6 21 // hum-temp - 2

//colonne3
#define address7 27 // co2 - 3
#define address8 47 // oo2 - 3
#define address9 20 // hum - 3 


#define NUM_SENSORS 9
#define NUM_GAS_SENSORS 9
#define I2C_ADDRESS 0
#define TYPE_OF_SENSOR 1



//MFS initialisation
//int analogPin = A3; // potentiometer wiper (middle terminal) connected to analog pin 3
//int val = 0; // variable to store the value read

//float raw_val;
//float filtered_val;
//float LPF = 0.05f;


int SENSOR_MAT[NUM_SENSORS][2] = {{105, 1}, {46, 2}, {22, 3},{28, 1}, {45, 2}, {21, 3}, {27, 1}, {47, 2},{20, 3}};
String SENSOR_CODE[NUM_SENSORS] = {"CO2_1 : ", "OO2_1 : ", "HUM_1 : ","CO2_2 : ", "OO2_2 : ", "HUM_2 : ", "CO2_3 : ", "OO2_3 : ", "HUM_3 : "};

//"MMS_1 : "};

char Message_to_gas_sensors[20];

byte received_from_computer = 0; //we need to know how many characters have been received.
byte serial_event = true; //a flag to signal when data has been received from the pc/mac/other.
byte code = 0; //used to hold the I2C response code.

char Data[20]; //we make a 20-byte character array to hold incoming data from the Co2 sensor.
byte in_char = 0; //used as a 1 byte buffer to store inbound bytes from the Co2 sensor.
byte i = 0; //counter used for Co2_data array.

int time_ = 600; //250 for co2, 600 for 02 //used to set the delay needed to process the command sent to the EZO Co2 sensor.
int Co2_int; //int var used to hold the value of the Co2.
bool I2c_success = true;
int counter_gassens_loop = 0;
int counter_gassens_threshold = 20;
int id = 0;

//float o2_float;

void setup() //hardware initialization.
{

  Serial.begin(9600); //enable serial port.
  Wire.begin(); //enable I2C port. 

}


void loop() {

  Message_to_gas_sensors[0] = 'r'; //code to tell the gas sensor to take a reading
  counter_gassens_loop ++;


  if (counter_gassens_loop == counter_gassens_threshold)
  {

    counter_gassens_loop = 0; 
    id ++;

    if(id ==NUM_GAS_SENSORS)
    {
      id = 0;
    }

    Wire.beginTransmission(SENSOR_MAT[id][I2C_ADDRESS]);
    Wire.write(Message_to_gas_sensors);
    Wire.endTransmission();


    switch( SENSOR_MAT[id][TYPE_OF_SENSOR] ){
      case 1:
      time_ = 250;
      break;
      case 2:
      time_ = 600;
      break;
      case 3:
      time_ = 300;
      break;
    }


    delay(time_) ; //wait the correct amount of time for the circuit to complete its instruction.
    Wire.requestFrom(SENSOR_MAT[id][I2C_ADDRESS], 20, 1) ;
    code = Wire.read() ;



    switch (code) 
    { //switch case based on what the response code is.

      case 1: //decimal 1.
      //Serial.println("Success"); //means the command was successful.
      I2c_success = true;
      break; //exits the switch case.

      case 2: //decimal 2.
      Serial.println("B" + SENSOR_CODE[id] + "Failed" + "F"); //means the command has failed.
      I2c_success = false;
      break; //exits the switch case.

      case 254: //decimal 254.
      Serial.println("B" + SENSOR_CODE[id] + "Pending" + "F"); //means the command has not yet been finished calculating.
      I2c_success = false;
      break; //exits the switch case.

      case 255: //decimal 255. 
      Serial.println("B" + SENSOR_CODE[id] + "No Data" + "F"); //means there is no further data to send. 
      I2c_success = false;
      break;

    } 

    while (Wire.available()) 
    { 
      //are there bytes to receive.
      in_char = Wire.read(); //receive a byte.
      Data[i] = in_char; //load this byte into our array.
      i += 1; //incur the counter for the array element.

      if (in_char == 0) 
      { //if we see that we have been sent a null command.
        i = 0; //reset the counter i to 0.
        break; //exit the while loop.
      }

    } 

    if(I2c_success)
    {
      Serial.println("B" + SENSOR_CODE[id] + Data + "F"); //print the data.
      Data[0] = 0;
    }

  }

  //val = analogRead(analogPin); // read the input pin
  //raw_val = (float) val;
  //filtered_val = ( 1.0f - LPF ) * filtered_val + LPF * raw_val;
  //Serial.println("B" + SENSOR_CODE[NUM_GAS_SENSORS] + filtered_val + "F"); //print the data.

  serial_event = false; //reset the serial event flag

}


