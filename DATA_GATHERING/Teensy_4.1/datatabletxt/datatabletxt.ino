#include <SD.h>
#include <SPI.h>
#include <Wire.h>

const int chipSelect = BUILTIN_SDCARD; // Use the built-in SD card slot's default CS pin
const char *filename = "datatable3.txt"; // Name of the text file to be created
const unsigned long duration = 60000; // Run for 60 seconds (60,000 milliseconds)

bool minuteElapsed = false; // Flag to indicate whether a minute has elapsed

void setup() {
  Serial.begin(9600);

  // Initialize SD card with the specified chip select pin
  if (SD.begin(chipSelect)) {
    Serial.println("SD Card initialized successfully.");

    // Open the file in write mode
    File file = SD.open(filename, FILE_WRITE);

    // Check if the file is opened successfully
    if (file) {
      Serial.println("Writing data to file...");

      file.print("Pressure_target");
      file.print(",Pressure");
      file.print(",Time");
      file.print(",FSR");
      file.println(",Cycles");

      // Close the file
      file.close();

      Serial.println("File created.");
    } else {
      Serial.println("Error opening the file.");
    }
  } else {
    Serial.println("SD Card initialization failed!");
  }
}

void loop() {
  unsigned long startTime = millis();

  while (millis() - startTime < duration) {
    // Open the file in append mode
    File file = SD.open(filename, FILE_WRITE);

    // Check if the file is opened successfully
    if (file) {
      // Generate random values for each variable
      float pressureTarget = random(0, 101); // Random value between 0 and 100
      float pressure = random(0, 101); // Random value between 0 and 100
      unsigned long currentTime = millis(); // Current time in milliseconds
      int fsr = random(0, 1024); // Random value between 0 and 1023 for FSR
      int cycles = random(1, 101); // Random value between 1 and 100 for cycles

      // Write the data to the file
      file.print(pressureTarget);
      file.print(" , ");
      file.print(pressure);
      file.print(" , ");
      file.print(currentTime);
      file.print(" , ");
      file.print(fsr);
      file.print(" , ");
      file.println(cycles);

      // Close the file
      file.close();

      Serial.println("Data line written to file.");
    } else {
      Serial.println("Error opening the file.");
    }

    // Wait for a second (1000 milliseconds) before writing the next line
    delay(1000);
  }

  // Set the flag to indicate that a minute has elapsed
  minuteElapsed = true;

  // Wait indefinitely after writing for a minute
  while (minuteElapsed) {
    // Do nothing
  }
}