// ADC and potentiometer ( = analog input )

int potPin = A0 ;
int potVal ;

// PWM ( = analog output )
int PWM_pin = 9 ; 
int duty_cycle ; // duty cycle corresponds to the proportion during which the PWM signal is HIGH ( 5V )

void setup() {
  // put your setup code here, to run once:

  pinMode(potPin, INPUT) ; // declare the ADC pin on which is connected to potentiometer as input 
  pinMode(PWM_pin, OUTPUT) ; // declare the PWM pin as output 

}

void loop() {
  // put your main code here, to run repeatedly:

  potVal = analogRead( potPin ) ; // triggers an ADC conversion to read the voltage at the A0 pin 
  duty_cycle = map( potVal , 0 , 1023 , 0 , 255 ) ; /* convert the 10bit ADC value (here: potVal)
  into an 8bit value (the PWM signal has a precision of 8bit), using the map function (linear interpolation) */

  analogWrite( PWM_pin , duty_cycle ) ; // output a PWM signal (for example: to trim an LED)

}