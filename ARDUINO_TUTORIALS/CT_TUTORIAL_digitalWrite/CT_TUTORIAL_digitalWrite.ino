
// variables storing the LED and BUTTON pins
int LED_PIN = 13 ; 

bool state ; // boolean type can be either 0 or 1

void setup() {
  // put your setup code here, to run once:

	pinMode(LED_PIN, OUTPUT) ; 
	// LED pin declared as an output pin to control an LED

}

void loop() {

  // put your main code here, to run repeatedly:

	state = !state ;
	
	digitalWrite( LED_PIN, state ) ; 
  /* control the LED, if state = 0
	the LED is switched off, if state = 1 tke LED is switched on. */
	
	delay(1000) ; // delay of 1000 milliseconds 

}