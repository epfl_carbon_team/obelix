// variables storing the LED and BUTTON pins
int LED_PIN = 13 ; 
int BUTTON_PIN = 9 ;

bool state ;

void setup() {
  // put your setup code here, to run once:
	pinMode(LED_PIN, OUTPUT) ; 
	
	pinMode(BUTTON_PIN, INPUT_PULLUP ) ; 
	/* must be input pullup because
	when the button is at rest the pin is floating (HI-Z state) 
	and the state is incertain. By putting a pull-up resistor
	the state will be by default HIGH ( = 1 ) */
}

void loop() {

  // put your main code here, to run repeatedly:
	
	state = digitalRead(BUTTON_PIN) ; 
	/* if pin 9 (BUTTON_PIN) is connected to 
	ground, state will be equal to 0. if pin 9 is connected to 5V, state
	will be equal to 1. */
	
	digitalWrite(LED_PIN, state ) ; 
	/* control the LED, if state = 0
	the LED is switched off, if state = 1 tke LED is switched on. */

}
