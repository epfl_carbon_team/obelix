#define TWO_PI 6.28318530718f // define 2 * pi to spare the CPU from multiplying pi by 2

float theta ;
float A = TWO_PI / 2.0f ;
float wave ; 

void setup(){

  Serial.begin(9600) ;
  /* Serial communciation enabled, 
  the baud rate (9600) corresponds to the number
  of signal elements transmitted in one second*/

}

void loop (){

  // MATHS. 
  theta += 0.3f ;

  if( theta > TWO_PI ){
    theta -= TWO_PI ;
  }

  wave = A * sin( theta ) + A ; 

  // SERIAL PRINT 
  Serial.print( theta ) ;
  Serial.print( " , " ) ;
  Serial.println( wave ) ; // ALWAYS END WITH Serial.println

}