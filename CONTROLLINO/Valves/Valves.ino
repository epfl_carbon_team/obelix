#include <SPI.h>
#include <Controllino.h>

bool VALVES[3][4];
long CYCLE = 10000;
byte COLUMN[3] = {2,1,0};

void setup() {
  pinMode(CONTROLLINO_R0,  OUTPUT);
  pinMode(CONTROLLINO_R1,  OUTPUT);
  pinMode(CONTROLLINO_R2,  OUTPUT);
  pinMode(CONTROLLINO_R3,  OUTPUT);
  pinMode(CONTROLLINO_R4,  OUTPUT);
  pinMode(CONTROLLINO_R5,  OUTPUT);
  pinMode(CONTROLLINO_R6,  OUTPUT);
  pinMode(CONTROLLINO_R7,  OUTPUT);
  pinMode(CONTROLLINO_R8,  OUTPUT);
  pinMode(CONTROLLINO_R9,  OUTPUT);
  pinMode(CONTROLLINO_R10, OUTPUT);
  pinMode(CONTROLLINO_R11, OUTPUT);

  for (int i=0; i<3; i++) {
    for (int j=0; j<4; j++) {
      VALVES[i][j] = false;
    }
  }

  //Absorption
  VALVES[0][0] = true;
  VALVES[0][2] = true;

  digitalWrite(CONTROLLINO_R0,  VALVES[0][0]);
  digitalWrite(CONTROLLINO_R1,  VALVES[0][1]);
  digitalWrite(CONTROLLINO_R2,  VALVES[0][2]);
  digitalWrite(CONTROLLINO_R3,  VALVES[0][3]);
  digitalWrite(CONTROLLINO_R4,  VALVES[1][0]);
  digitalWrite(CONTROLLINO_R5,  VALVES[1][1]);
  digitalWrite(CONTROLLINO_R6,  VALVES[1][2]);
  digitalWrite(CONTROLLINO_R7,  VALVES[1][3]);
  digitalWrite(CONTROLLINO_R8,  VALVES[2][0]);
  digitalWrite(CONTROLLINO_R9,  VALVES[2][1]);
  digitalWrite(CONTROLLINO_R10, VALVES[2][2]);
  digitalWrite(CONTROLLINO_R11, VALVES[2][3]);

  delay(CYCLE);

  //Absorption
  VALVES[1][0] = true;
  VALVES[1][2] = true;
  //Heating
  VALVES[0][0] = false;
  VALVES[0][2] = false;

  digitalWrite(CONTROLLINO_R0,  VALVES[0][0]);
  digitalWrite(CONTROLLINO_R1,  VALVES[0][1]);
  digitalWrite(CONTROLLINO_R2,  VALVES[0][2]);
  digitalWrite(CONTROLLINO_R3,  VALVES[0][3]);
  digitalWrite(CONTROLLINO_R4,  VALVES[1][0]);
  digitalWrite(CONTROLLINO_R5,  VALVES[1][1]);
  digitalWrite(CONTROLLINO_R6,  VALVES[1][2]);
  digitalWrite(CONTROLLINO_R7,  VALVES[1][3]);
  digitalWrite(CONTROLLINO_R8,  VALVES[2][0]);
  digitalWrite(CONTROLLINO_R9,  VALVES[2][1]);
  digitalWrite(CONTROLLINO_R10, VALVES[2][2]);
  digitalWrite(CONTROLLINO_R11, VALVES[2][3]);
  
  delay(CYCLE);
}

void loop() {
  // Absorption
  VALVES[COLUMN[0]][0] = true;
  VALVES[COLUMN[0]][2] = true;
  VALVES[COLUMN[0]][1] = false;
  VALVES[COLUMN[0]][3] = false;

  //Heating
  VALVES[COLUMN[1]][0] = false;
  VALVES[COLUMN[1]][2] = false;

  //Desorption
  VALVES[COLUMN[2]][1] = true;
  VALVES[COLUMN[2]][3] = true;

  //Columns change
  for (int i=0; i<3; i++) {
    COLUMN[i]++;
    if (COLUMN[i]>2) {
      COLUMN[i] = 0;
    }
  }

  digitalWrite(CONTROLLINO_R0,  VALVES[0][0]);
  digitalWrite(CONTROLLINO_R1,  VALVES[0][1]);
  digitalWrite(CONTROLLINO_R2,  VALVES[0][2]);
  digitalWrite(CONTROLLINO_R3,  VALVES[0][3]);
  digitalWrite(CONTROLLINO_R4,  VALVES[1][0]);
  digitalWrite(CONTROLLINO_R5,  VALVES[1][1]);
  digitalWrite(CONTROLLINO_R6,  VALVES[1][2]);
  digitalWrite(CONTROLLINO_R7,  VALVES[1][3]);
  digitalWrite(CONTROLLINO_R8,  VALVES[2][0]);
  digitalWrite(CONTROLLINO_R9,  VALVES[2][1]);
  digitalWrite(CONTROLLINO_R10, VALVES[2][2]);
  digitalWrite(CONTROLLINO_R11, VALVES[2][3]);

  delay(CYCLE);
}
